package ru.tsc.avramenko.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.tsc.avramenko.tm.exception.entity.UserNotFoundException;
import ru.tsc.avramenko.tm.exception.system.ProcessException;
import ru.tsc.avramenko.tm.model.User;

import java.util.List;

public class UserRepositoryTest {

    @Nullable
    private UserRepository userRepository;

    @Nullable
    private User user;

    @NotNull
    protected static final String TEST_USER_LOGIN = "TestLogin";

    @NotNull
    protected static final String TEST_USER_EMAIL = "TestEmail";

    @Before
    public void before() {
        userRepository = new UserRepository();
        @NotNull final User user = new User();
        user.setLogin(TEST_USER_LOGIN);
        user.setEmail(TEST_USER_EMAIL);
        this.user = userRepository.add(user);
    }

    @Test
    public void add() {
        Assert.assertNotNull(user);
        Assert.assertNotNull(user.getId());
        Assert.assertNotNull(user.getLogin());
        Assert.assertEquals(TEST_USER_LOGIN, user.getLogin());
        Assert.assertNotNull(user.getEmail());
        Assert.assertEquals(TEST_USER_EMAIL, user.getEmail());

        @Nullable final User userById = userRepository.findById(user.getId());
        Assert.assertNotNull(userById);
        Assert.assertEquals(user, userById);
    }

    @Test
    public void findAll() {
        @Nullable final List<User> users = userRepository.findAll();
        Assert.assertEquals(1, users.size());
    }

    @Test
    public void findById() {
        @Nullable final User user = userRepository.findById(this.user.getId());
        Assert.assertNotNull(user);
    }

    @Test
    public void findByIdIncorrect() {
        @Nullable final User user = userRepository.findById("647");
        Assert.assertNull(user);
    }

    @Test
    public void findByIdNull() {
        @Nullable final User user = userRepository.findById(null);
        Assert.assertNull(user);
    }

    @Test
    public void findByLogin() {
        @Nullable final User user = userRepository.findByLogin(this.user.getLogin());
        Assert.assertNotNull(user);
    }

    @Test
    public void findByLoginIncorrect() {
        @Nullable final User user = userRepository.findByLogin("647");
        Assert.assertNull(user);
    }

    @Test
    public void findByLoginNull() {
        @Nullable final User user = userRepository.findByLogin(null);
        Assert.assertNull(user);
    }

    @Test
    public void findByEmail() {
        @Nullable final User user = userRepository.findByEmail(this.user.getEmail());
        Assert.assertNotNull(user);
    }

    @Test
    public void findByEmailIncorrect() {
        @Nullable final User user = userRepository.findByEmail("647");
        Assert.assertNull(user);
    }

    @Test
    public void findByEmailNull() {
        @Nullable final User user = userRepository.findByEmail(null);
        Assert.assertNull(user);
    }

    @Test
    public void remove() {
        userRepository.removeById(user.getId());
        Assert.assertNull(userRepository.findById(user.getId()));
    }

    @Test
    public void removeById() {
        userRepository.removeById(user.getId());
        Assert.assertNull(userRepository.findById(user.getId()));
    }

    @Test
    public void removeByIdNull() {
        Assert.assertNull(userRepository.removeById(null));
    }

    @Test
    public void removeByIdIncorrect() {
        @Nullable final User user = userRepository.removeById("647");
        Assert.assertNull(user);
    }

    @Test
    public void removeUserByLogin() {
        @Nullable final User user = userRepository.removeUserByLogin(this.user.getLogin());
        Assert.assertNotNull(user);
    }

    @Test
    public void removeUserByLoginIncorrect() {
        @Nullable final User user = userRepository.removeUserByLogin("647");
        Assert.assertNull(user);
    }

    @Test
    public void removeUserByLoginNull() {
        @Nullable final User user = userRepository.removeUserByLogin(null);
        Assert.assertNull(user);
    }

}