package ru.tsc.avramenko.tm.exception.empty;

import ru.tsc.avramenko.tm.exception.AbstractException;

public class EmptyIdException extends AbstractException {

    public EmptyIdException() {
        super("Error! Id is empty.");
    }

    public EmptyIdException(String value) {
        super("Error! `" + value + "` is empty.");
    }

}