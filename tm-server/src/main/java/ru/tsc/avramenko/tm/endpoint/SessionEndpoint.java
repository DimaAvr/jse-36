package ru.tsc.avramenko.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.avramenko.tm.api.endpoint.ISessionEndpoint;
import ru.tsc.avramenko.tm.api.service.IServiceLocator;
import ru.tsc.avramenko.tm.exception.system.AccessDeniedException;
import ru.tsc.avramenko.tm.model.Session;
import ru.tsc.avramenko.tm.model.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public final class SessionEndpoint extends AbstractEndpoint implements ISessionEndpoint {

    public SessionEndpoint() {
        super(null);
    }

    public SessionEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Nullable
    @WebMethod
    public Session openSession(
            @WebParam(name = "login", partName = "login") final String login,
            @WebParam(name = "password", partName = "password") final String password
    ) {
        return serviceLocator.getSessionService().open(login, password);
    }

    @Nullable
    @WebMethod
    public boolean closeSession(
            @WebParam(name = "session", partName = "session") final Session session
    ) {
        serviceLocator.getSessionService().validate(session);
        try {
            serviceLocator.getSessionService().close(session);
            return true;
        } catch (@NotNull final AccessDeniedException e) {
            return false;
        }
    }

}