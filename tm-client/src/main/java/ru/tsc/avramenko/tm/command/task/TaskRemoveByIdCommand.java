package ru.tsc.avramenko.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.avramenko.tm.command.AbstractTaskCommand;
import ru.tsc.avramenko.tm.endpoint.Role;
import ru.tsc.avramenko.tm.endpoint.Session;
import ru.tsc.avramenko.tm.endpoint.Task;
import ru.tsc.avramenko.tm.exception.entity.TaskNotFoundException;
import ru.tsc.avramenko.tm.exception.system.AccessDeniedException;
import ru.tsc.avramenko.tm.util.TerminalUtil;

import java.util.Optional;

public class TaskRemoveByIdCommand extends AbstractTaskCommand {

    @NotNull
    @Override
    public String name() {
        return "task-remove-by-id";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Remove task by id.";
    }

    @Override
    public void execute() {
        @Nullable final Session session = serviceLocator.getSessionService().getSession();
        Optional.ofNullable(session).orElseThrow(AccessDeniedException::new);
        System.out.println("ENTER ID:");
        @Nullable final String id = TerminalUtil.nextLine();
        @NotNull final Task task = serviceLocator.getTaskEndpoint().removeTaskById(session, id);
        if (task == null) throw new TaskNotFoundException();
    }

    @Nullable
    @Override
    public Role[] roles() {
        return Role.values();
    }

}